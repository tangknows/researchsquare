<?php

    class ResearchSquare {

        const RESEARCH_STRING = "Reserch \n";
        const SQUARE_STRING = "Square \n";
        const RESEARCH_SQUARE_STRING = "Research_Square \n";

        public function run()
        {
            for($i=0; $i<100;$i++){
                if ($i == 0){
                    continue;
                }

                $isMultipleOfThree = ($i % 3 == 0);
                $isMultipleOfFive = ($i % 5 == 0);

                if ($isMultipleOfThree && $isMultipleOfFive){
                    echo self::RESEARCH_SQUARE_STRING;
                }
                else if ($isMultipleOfThree){        
                    echo self::RESEARCH_STRING;
                }
                else if ($isMultipleOfFive){
                    echo self::SQUARE_STRING;
                }
                        
            }
        }

    }

    $researchSquare = new ResearchSquare();
    $researchSquare->run();
?>


